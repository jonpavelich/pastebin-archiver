# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Added
- Type hinting throughout the project so that strict type checking (mypy) passes
- Wrote a Dockerfile, you can now run `docker build .` from source
- Extra dependency groups for database support (mysql, mssql) when installing from source

## [0.2.0] - 2019-11-19
### Added
- This changelog (including descriptions of previous releases)

### Changed
- Switched from Pipenv to Poetry for dependency management and packaging
- Some log messages have been cleaned up and formatted a little bit nicer

### Fixed
- A bug which caused the app to crash when encountering an expired metadata record that has not been fetched

## [0.1.1] - 2019-09-26
### Changed
- PyPI package description

## [0.1.0] - 2019-09-26
### Added
- The entire project! This is the first official release and changes are not tracked prior to v0.1.0

[unreleased]: https://gitlab.com/jonpavelich/pastebin-archiver/compare/v0.2.0...master
[0.2.0]: https://gitlab.com/jonpavelich/pastebin-archiver/compare/v0.1.1...v0.2.0
[0.1.1]: https://gitlab.com/jonpavelich/pastebin-archiver/compare/v0.1.0...v0.1.1
[0.1.0]: https://gitlab.com/jonpavelich/pastebin-archiver/tree/v0.1.0
