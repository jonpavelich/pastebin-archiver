import unittest
from datetime import datetime, timedelta
from typing import Optional
from unittest import mock

from requests.exceptions import HTTPError

from .context import pastebin_archiver
from .helpers import TestHelpers


def mocked_requests_get(*args, **kwargs):
    class MockResponse:
        def __init__(
            self,
            status_code: int,
            json_data: Optional[dict] = None,
            text_data: Optional[str] = None,
        ):
            self.json_data = json_data
            self.text = text_data
            self.status_code = status_code

        def json(self):
            return self.json_data

        def raise_for_status(self):
            if 400 <= self.status_code < 600:
                raise HTTPError(f"Mocked HTTP Error (status {self.status_code})")
            return self

    if str(args[0]).startswith("https://scrape.pastebin.com/api_scrape_item.php"):
        return MockResponse(200, text_data=TestHelpers.sample_content()["body"])
    elif str(args[0]).startswith("https://scrape.pastebin.com/api_scraping.php"):
        return MockResponse(200, json_data=TestHelpers.sample_api_metadata_response())
    return MockResponse(404)


class TestApi(unittest.TestCase):
    def setUp(self):
        self.api = pastebin_archiver.API()
        self.database = self.api.database
        self.helpers = TestHelpers(self.database)

    @mock.patch("pastebin_archiver.api.requests.get", side_effect=mocked_requests_get)
    def test_content_valid_record_fetched(self, mocked_get):
        now = datetime.utcnow()
        record = self.helpers.insert_sample_metadata(
            date=now, expire=now + timedelta(hours=1)
        )
        content = self.api.fetch_content(record)
        self.assertIsNotNone(content)
        self.assertEqual(record.id, content.id)

    @mock.patch("pastebin_archiver.api.requests.get", side_effect=mocked_requests_get)
    def test_content_expired_record_not_fetched(self, mocked_get):
        now = datetime.utcnow()
        record = self.helpers.insert_sample_metadata(
            date=now, expire=now - timedelta(hours=1)
        )
        content = self.api.fetch_content(record)
        self.assertIsNone(content)

    @mock.patch("pastebin_archiver.api.requests.get", side_effect=mocked_requests_get)
    def test_metadata_fetched(self, mocked_get):
        records = self.api.fetch_metadata()
        self.assertIsNotNone(records)
        self.assertTrue(len(records) > 0)

    @mock.patch("pastebin_archiver.api.requests.get", side_effect=mocked_requests_get)
    def test_metadata_one_existing_not_fetched(self, mocked_get):
        sample_response = self.helpers.sample_api_metadata_response()
        earliest = min(sample_response, key=lambda x: x.get("date"))
        self.helpers.insert_metadata(pastebin_archiver.models.Metadata(**(earliest)))
        records = self.api.fetch_metadata()
        self.assertLess(len(records), len(sample_response))
        self.assertNotIn(earliest["date"], [x.date for x in records])

    @mock.patch("pastebin_archiver.api.requests.get", side_effect=mocked_requests_get)
    def test_metadata_all_existing_not_fetched(self, mocked_get):
        sample_response = self.helpers.sample_api_metadata_response()
        latest = max(sample_response, key=lambda x: x.get("date"))
        self.helpers.insert_metadata(pastebin_archiver.models.Metadata(**(latest)))
        records = self.api.fetch_metadata()
        self.assertEqual(len(records), 0)


if __name__ == "__main__":
    unittest.main()
