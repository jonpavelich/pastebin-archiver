import unittest

from .context import pastebin_archiver


class TestEventQueue(unittest.TestCase):
    def sample_func_hello_world(self):
        return "Hello world"

    def sample_func_hello_name(self, name):
        return f"Hello {name}"

    def setUp(self):
        pastebin_archiver.EventQueue.reset()

    def test_init_empty(self):
        q = pastebin_archiver.EventQueue()
        self.assertEqual(q.next(), None)

    def test_enqueue_simple(self):
        q = pastebin_archiver.EventQueue()
        q.enqueue(self.sample_func_hello_world)
        self.assertEqual("Hello world", q.next().run())

    def test_enqueue_param(self):
        q = pastebin_archiver.EventQueue()
        q.enqueue(self.sample_func_hello_world, {}, False)
        self.assertEqual("Hello world", q.next().run())

    def test_enqueue_multiple_default_priority(self):
        q = pastebin_archiver.EventQueue()
        q.enqueue(self.sample_func_hello_name, {"name": "A"})
        q.enqueue(self.sample_func_hello_name, {"name": "B"})
        q.enqueue(self.sample_func_hello_name, {"name": "C"})
        self.assertEqual("Hello A", q.next().run())
        self.assertEqual("Hello B", q.next().run())
        self.assertEqual("Hello C", q.next().run())

    def test_enqueue_multiple_low(self):
        q = pastebin_archiver.EventQueue()
        q.enqueue(self.sample_func_hello_world, {}, False)
        q.enqueue(self.sample_func_hello_name, {"name": "John"}, False)
        self.assertEqual("Hello world", q.next().run())
        self.assertEqual("Hello John", q.next().run())
        self.assertEqual(None, q.next())

    def test_enqueue_multiple_high(self):
        q = pastebin_archiver.EventQueue()
        q.enqueue(self.sample_func_hello_world, {}, True)
        q.enqueue(self.sample_func_hello_name, {"name": "John"}, True)
        self.assertEqual("Hello John", q.next().run())
        self.assertEqual("Hello world", q.next().run())
        self.assertEqual(None, q.next())

    def test_enqueue_multiple_mixed(self):
        q = pastebin_archiver.EventQueue()
        q.enqueue(self.sample_func_hello_name, {"name": "B"}, False)
        q.enqueue(self.sample_func_hello_name, {"name": "A"}, True)
        q.enqueue(self.sample_func_hello_name, {"name": "C"}, False)
        self.assertEqual("Hello A", q.next().run())
        self.assertEqual("Hello B", q.next().run())
        self.assertEqual("Hello C", q.next().run())
        self.assertEqual(None, q.next())


if __name__ == "__main__":
    unittest.main()
