import unittest
from datetime import datetime, timedelta
from unittest import mock

import apscheduler

from .context import pastebin_archiver
from .helpers import TestHelpers


class TestQueryMissingContent(unittest.TestCase):
    def setUp(self):
        self.app = pastebin_archiver.PastebinArchiver()
        self.helpers = TestHelpers(self.app.database)

    def test_query_missing_content_empty(self):
        records = self.app.query_missing_content()
        self.assertEqual(0, len(records))

    def test_query_missing_content_includes_valid(self):
        count = 3
        now = datetime.utcnow()
        for x in range(count):
            self.helpers.insert_sample_metadata(
                date=now - timedelta(hours=1), expire=now + timedelta(hours=1 + x)
            )
        records = self.app.query_missing_content()
        self.assertEqual(count, len(records))

    def test_query_missing_content_excludes_expired(self):
        count = 3
        now = datetime.utcnow()
        for x in range(count):
            self.helpers.insert_sample_metadata(
                date=now - timedelta(hours=2 + x), expire=now - timedelta(hours=1 + x)
            )
        records = self.app.query_missing_content()
        self.assertEqual(0, len(records))

    def test_query_missing_content_excludes_content(self):
        count = 3
        now = datetime.utcnow()
        for x in range(count):
            metadata = self.helpers.insert_sample_metadata(
                date=now - timedelta(hours=1), expire=now + timedelta(hours=1 + x)
            )
            self.helpers.insert_sample_content(metadata)
        records = self.app.query_missing_content()
        self.assertEqual(0, len(records))

    def test_query_missing_content_mixed_records(self):
        count = 3
        now = datetime.utcnow()
        for x in range(count):
            # insert valid record without content
            self.helpers.insert_sample_metadata(
                date=now - timedelta(hours=1), expire=now + timedelta(hours=1 + x)
            )
            # insert expired record without content
            self.helpers.insert_sample_metadata(
                date=now - timedelta(hours=2 + x), expire=now - timedelta(hours=1 + x)
            )
            # insert valid record with content
            metadata = self.helpers.insert_sample_metadata(
                date=now - timedelta(hours=1), expire=now + timedelta(hours=1 + x)
            )
            self.helpers.insert_sample_content(metadata)
        records = self.app.query_missing_content()
        self.assertEqual(count, len(records))

    def test_query_missing_content_sort_order(self):
        now = datetime.utcnow()
        order = [3, 2, 4, 1]
        for x in order:
            self.helpers.insert_sample_metadata(
                date=now, expire=now + timedelta(hours=x)
            )
        records = self.app.query_missing_content()
        self.assertEqual(len(order), len(records))
        sorted_order = sorted(order)
        for i in range(len(order)):
            self.assertEqual(records[i].expire, now + timedelta(hours=sorted_order[i]))


class TestStartup(unittest.TestCase):
    def setUp(self):
        self.app = pastebin_archiver.PastebinArchiver()
        self.helpers = TestHelpers(self.app.database)

    @mock.patch("pastebin_archiver.PastebinArchiver.event_loop", return_value=None)
    def test_startup_missing_content_enqueued(self, event_loop_mock):
        with mock.MagicMock() as query_missing_content_mock:
            self.app.query_missing_content = query_missing_content_mock
            self.app.main()
            query_missing_content_mock.assert_called()

    @mock.patch("pastebin_archiver.PastebinArchiver.event_loop", return_value=None)
    def test_startup_metadata_scheduled_immediately(self, event_loop_mock):
        with mock.MagicMock() as event_queue_mock:
            self.app.event_queue = event_queue_mock
            self.app.main()
            event_queue_mock.enqueue.assert_called_with(self.app.api.fetch_metadata)

    @mock.patch("pastebin_archiver.PastebinArchiver.event_loop", return_value=None)
    def test_startup_metadata_scheduled_periodically(self, event_loop_mock):
        with mock.MagicMock() as scheduler_mock:
            self.app.scheduler = scheduler_mock
            self.app.main()
            scheduler_mock.start.assert_called_once()
            scheduler_mock.add_job.assert_called_once()
            self.assertIn("func", scheduler_mock.add_job.call_args[1])
            self.assertEqual(
                scheduler_mock.add_job.call_args[1]["func"],
                self.app.event_queue.enqueue,
            )
            self.assertIsInstance(
                scheduler_mock.add_job.call_args[1]["trigger"],
                apscheduler.triggers.interval.IntervalTrigger,
            )

    @mock.patch("pastebin_archiver.PastebinArchiver.event_loop", return_value=None)
    def test_event_loop_started(self, mock):
        self.app.main()
        mock.assert_called_once()


if __name__ == "__main__":
    unittest.main()
