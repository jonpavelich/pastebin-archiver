from dataclasses import dataclass
from datetime import datetime, timedelta, timezone
from typing import Any, Dict, List, Optional

import pastebin_archiver.models as models

from .context import pastebin_archiver


@dataclass
class TestHelpers:
    """
    Reusable helpers for unit testing. Class methods do not require a database instance.

    :param database: Database instance used to write records
    :param now: datetime to use as a static current time for sample data (default is also static)
    """

    database: pastebin_archiver.Database
    now: datetime = datetime.utcnow()

    @classmethod
    def to_utc_timestamp(cls, datetime: datetime) -> int:
        return int(datetime.replace(tzinfo=timezone.utc).timestamp())

    @classmethod
    def sample_metadata(
        cls, override: Optional[Dict[str, Any]] = None
    ) -> Dict[str, Any]:
        """
        Provides an example metadata record as a dict

        :param override: an optional dict of overrides to apply to the default data
        :return: a dict containing example data, with overrides applied
        """
        data = {
            "key": "0CeaNm8Y",
            "date": cls.to_utc_timestamp(cls.now - timedelta(hours=1)),
            "size": 890,
            "expire": cls.to_utc_timestamp(cls.now + timedelta(hours=1)),
            "title": "This is an example title",
            "syntax": "python",
            "user": "admin",
            "scrape_url": "https://scrape.pastebin.com/api_scrape_item.php?i=0CeaNm8Y",
            "full_url": "https://pastebin.com/0CeaNm8Y",
        }
        if override:
            for key in override:
                data[key] = override[key]
        return data

    @classmethod
    def sample_content(cls) -> Dict[str, str]:
        """
        Provides an example content record as a dict

        :return: a dict containing example data
        """
        return {"body": "This is an example post body"}

    @classmethod
    def sample_api_metadata_response(cls) -> List[Dict[str, Any]]:
        """
        Provides an example API response of sample metadata records

        :return: an example JSON reponse as a list of dicts
        """
        data = [
            {
                "scrape_url": "https://scrape.pastebin.com/api_scrape_item.php?i=0CeaNm8Y",
                "full_url": "https://pastebin.com/0CeaNm8Y",
                "date": cls.to_utc_timestamp(cls.now - timedelta(hours=1)),
                "key": "0CeaNm8Y",
                "size": "890",
                "expire": cls.to_utc_timestamp(cls.now + timedelta(hours=3)),
                "title": "Once we all know when we goto function",
                "syntax": "java",
                "user": "admin",
            },
            {
                "scrape_url": "https://scrape.pastebin.com/api_scrape_item.php?i=8sUIsf34",
                "full_url": "https://pastebin.com/8sUIsf34",
                "date": cls.to_utc_timestamp(cls.now - timedelta(hours=2)),
                "key": "8sUIsf34",
                "size": "250",
                "expire": cls.to_utc_timestamp(cls.now - timedelta(hours=1)),
                "title": "master / development delete restriction",
                "syntax": "php",
                "user": "",
            },
            {
                "scrape_url": "https://scrape.pastebin.com/api_scrape_item.php?i=tFJtadnV",
                "full_url": "https://pastebin.com/tFJtadnV",
                "date": cls.to_utc_timestamp(cls.now - timedelta(hours=3)),
                "key": "tFJtadnV",
                "size": "1230",
                "expire": cls.to_utc_timestamp(cls.now + timedelta(hours=2)),
                "title": "An example paste",
                "syntax": "",
                "user": "example",
            },
        ]
        return data

    def insert_metadata(self, metadata: models.Metadata) -> None:
        """
        Insert an existing Metadata object in to the database

        :param metadata: the Metadata object to insert to the database
        """
        session = self.database.Session()
        session.add(metadata)
        session.commit()

    def insert_sample_metadata(
        self, date: datetime, expire: datetime
    ) -> models.Metadata:
        """
        Insert a sample Metadata object (without content) in to the
        database with a specified expiry time

        :param date: the creation time for the Metadata object
        :param expire: the expire time for the Metadata object
        :return: a copy of the Metadata object which was inserted to the database
        """
        data = self.sample_metadata(override={"date": date, "expire": expire})
        metadata = pastebin_archiver.models.Metadata(**data)
        self.insert_metadata(metadata)
        return metadata

    def insert_content(self, content: models.Content) -> None:
        """
        Insert an existing Content object in to the database

        :param content: the Content object to insert to the database
        """
        session = self.database.Session()
        session.add(content)
        session.commit()

    def insert_sample_content(self, metadata: models.Metadata) -> models.Content:
        """
        Insert a Content object in to the database for a specified Metadata object

        :param metadata: the Metadata object to associate the Content with
        :return: a copy of the Content object which was inserted to the database
        """
        content = pastebin_archiver.models.Content(
            **self.sample_content(), id=metadata.id
        )
        self.insert_content(content)
        return content
