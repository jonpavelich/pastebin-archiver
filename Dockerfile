FROM python:3.7-slim AS base
LABEL maintainer="Jon Pavelich <docker@jonpavelich.com>"
ENV PYTHONFAULTHANDLER=1 \
    PYTHONHASHSEED=random \
    PYTHONUNBUFFERED=1

FROM base AS builder
WORKDIR /app
ENV PIP_DEFAULT_TIMEOUT=100 \
    PIP_DISABLE_PIP_VERSION_CHECK=1 \
    PIP_NO_CACHE_DIR=1
RUN pip install poetry
RUN apt update && apt install -y build-essential unixodbc-dev libpq-dev
ENV VIRTUAL_ENV=/venv
RUN python -m venv ${VIRTUAL_ENV}
ENV PATH="${VIRTUAL_ENV}/bin:${PATH}"
COPY pyproject.toml poetry.lock README.md ./
RUN poetry export -f requirements.txt -E pgsql -E mysql -E mssql | ${VIRTUAL_ENV}/bin/pip install -r /dev/stdin
COPY pastebin_archiver pastebin_archiver
RUN poetry build --format wheel
RUN ${VIRTUAL_ENV}/bin/pip install dist/*.whl

FROM base as final
COPY --from=builder /venv /venv
ENTRYPOINT [ "/venv/bin/pastebin-archiver" ]
